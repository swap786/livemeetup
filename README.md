# Java EE 7 - GlassFish WebSockets Chat Tutorial

An example setting up a websocket-enabled chat system using Java EE 7, custom encoders/decoders, GlassFish 4, the new Java API for JSON Processing and Bootstrap.

This application is LiveMeet Application by, using employees and team leaders in the office can communicate effectively, with each other.

For more detailed information, please feel free to have a look at my blog at [swapos.wordpress.com].

----

**2013 Swapnil U. / swapos.wordpress.com**

   [swapos.wordpress.com]:http://swapos.wordpress.com/
