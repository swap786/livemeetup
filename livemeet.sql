-- MySQL dump 10.14  Distrib 5.5.43-MariaDB, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: livemeet
-- ------------------------------------------------------
-- Server version	5.5.43-MariaDB-1ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `db_register`
--

DROP TABLE IF EXISTS `db_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db_register` (
  `usrname` varchar(25) NOT NULL DEFAULT '',
  `passwd` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dob` varchar(15) DEFAULT NULL,
  `team_leader` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`usrname`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `db_register`
--

LOCK TABLES `db_register` WRITE;
/*!40000 ALTER TABLE `db_register` DISABLE KEYS */;
INSERT INTO `db_register` VALUES ('chaitanya','dev','exavance@gmail.com','40/10/1991','swap'),('dev','dev','devchait@gmail.com','0404',NULL),('jhnony','swap','swapnil.udapure5@gmail.com','45453','xdev'),('jones','swap','swapnil.udapure5@gmail.com','545','swap'),('new','new','new@gmail','22545',NULL),('swap','swap','swap@gmail','04',NULL),('swapnil','swap','swapnil.udapure786@gmail.com','454',' xdev'),('swappy','swappy','swapnil@gmail.com','04',NULL);
/*!40000 ALTER TABLE `db_register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_session`
--

DROP TABLE IF EXISTS `login_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_session` (
  `usrname` varchar(50) NOT NULL DEFAULT '',
  `seesion_id` varchar(65) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_session`
--

LOCK TABLES `login_session` WRITE;
/*!40000 ALTER TABLE `login_session` DISABLE KEYS */;
INSERT INTO `login_session` VALUES ('jones','d22ca711d28d4df098304b776aaebadbf185fed6898e4d1cbece3ea5ceb40bd0'),('dev','e46db321b4604b5fba2a6e171876873504ae5b7dbaca48409906d5901563acdd'),('jones','6bece293e2ce4497a4734785f0edc7dd030ad7ba42eb42e4977f5980b640641f');
/*!40000 ALTER TABLE `login_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_leader_info`
--

DROP TABLE IF EXISTS `team_leader_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_leader_info` (
  `usrname` varchar(50) NOT NULL DEFAULT '',
  `passwd` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`usrname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_leader_info`
--

LOCK TABLES `team_leader_info` WRITE;
/*!40000 ALTER TABLE `team_leader_info` DISABLE KEYS */;
INSERT INTO `team_leader_info` VALUES ('crap','swap','swapnil.udapure786@gmail.com','sefsdf'),('dev','dev','dev@gmail','06/10/1995'),('devchait','dev','devchait@gmail.com','4545'),('swap','swap','swapnil.udapure786@gmail.com','04/10/1990'),('swappy','swappy','swappy@gmail','04/10/1993'),('xdev','xdev','xdev@gmail','06/14/1995');
/*!40000 ALTER TABLE `team_leader_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-01 10:53:30
