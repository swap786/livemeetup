/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.hascode.tutorial;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import java.util.UUID;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author swap
 */
public class test extends HttpServlet {

    public String Random64()
    {
        return UUID.randomUUID().toString().replaceAll("-", "")+UUID.randomUUID().toString().replaceAll("-", "");
    }


    public String SendMail(String mail_list, String msg, String sub) {
       String m_list [] =  mail_list.split(":");
       
       Client client = Client.create();
       client.addFilter(new HTTPBasicAuthFilter("api",
                       "key-7nc2d7e8uu7t13sezvor7yhcauaw-7t5"));
       WebResource webResource =
               client.resource("https://api.mailgun.net/v3/sandbox94047.mailgun.org" +
                               "/messages");
       MultivaluedMapImpl formData = new MultivaluedMapImpl();
       formData.add("from", "Excited User <mailgun@sandbox94047.mailgun.org>");
       for (String tmp : m_list)
       {
           formData.add("to", tmp.trim());
           System.out.println(tmp);
       }
       formData.add("subject", sub);
       formData.add("text", msg);
       if(webResource.type(MediaType.APPLICATION_FORM_URLENCODED).
               post(ClientResponse.class, formData).toString().contains("200 OK"))
       {
           return "ok";
       }
        else{
           return "no";
        }
       
}



    public int insertdata(String usrname,String passwd,String email,String dob,String teamleader) {
        String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "insert into db_register values("+"'"+usrname+"','"+passwd+"','"+email+"','"+dob+"','"+teamleader.trim()+"')";
        String username = "root";
        String password = "swap";
        String tableName = "";
        int i = 0;
        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            //ResultSet resultSet = statement.executeQuery(query);
            i = statement.executeUpdate(query);
            connection.close();
               
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
                 if(e.toString().contains("Duplicate entry"))
                {
                    i=4;
                }
            e.printStackTrace();
            }
        return i;
    }


    public int insertadmindata(String usrname,String passwd,String email,String dob) {
        String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "insert into team_leader_info values('"+usrname+"','"+passwd+"','"+email+"','"+dob+"')";
        String username = "root";
        String password = "swap";
        String tableName = "";
        int i = 0;
        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            //ResultSet resultSet = statement.executeQuery(query);
            i = statement.executeUpdate(query);
            connection.close();

            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
                if(e.toString().contains("Duplicate entry"))
                {
                    i=4;
                }
            e.printStackTrace();
            }
        return i;
    }

    
     public ArrayList<String> selectlist() {
        String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "select usrname from team_leader_info";
        String username = "root";
        String password = "swap";
        ArrayList<String> list = new ArrayList();
        
        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
            list.add(resultSet.getString(1));
            System.out.println("usrname: "+resultSet.getString(1));
            }
            connection.close();
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
            }
            return list;
    }

      public void addsession(String usrname,String session_id)
      {
      
          String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "insert into login_session values('"+usrname+"','"+session_id+"')";
        String username = "root";
        String password = "swap";
        int i = 0;
        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            //ResultSet resultSet = statement.executeQuery(query);
            i = statement.executeUpdate(query);
            connection.close();

            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
            }

      }


      public int logout(String session_id)
      {

          String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "delete from login_session where seesion_id='"+session_id+"'";
        String username = "root";
        String password = "swap";
        int i = 0;
        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            //ResultSet resultSet = statement.executeQuery(query);
            i = statement.executeUpdate(query);
            connection.close();

            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
            }
            return i;

      }

      public String selectadmindata(String usrname,String passwd) {
        String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "select * from team_leader_info where usrname="+"'"+usrname+"' and passwd='"+passwd+"'";
        String username = "root";
        String password = "swap";
        String status="";
        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            
            if (resultSet.next()) {
            if(resultSet.getString(1).equals(usrname) && resultSet.getString(2).equals(passwd))
            {
                String session = Random64();
                status = "exist:"+session;
             addsession(usrname,session);
            System.out.println("usrname: "+resultSet.getString(1)+" passwd: "+resultSet.getString(2));
            }
            else{
            status = "not";
            }

            }
            connection.close();
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
            }
            return status;
    }


      public String session_test(String session)
      {
           String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "select * from login_session where seesion_id="+"'"+session+"'";
        String username = "root";
        String password = "swap";
        String status="";
        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
            if(resultSet.getString(2).equals(session))
            {
                status = "valid"+":"+resultSet.getString(1);
                System.out.println(status);
            }
            else{
            status = "not";
            }

            }
            connection.close();
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
            }
            return status;
      }


       public ArrayList<String> fill_user(String usrname) {
        String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "select usrname,email from db_register where team_leader='"+usrname+"'";
        String username = "root";
        String password = "swap";
        ArrayList<String> list = new ArrayList();

        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
            list.add(resultSet.getString(1)+":"+resultSet.getString(2));
            System.out.println(resultSet.getString(1)+":"+resultSet.getString(2));
            }
            connection.close();
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
            }
            return list;
    }



        public ArrayList<String> get_user_list() {
        String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "select usrname from login_session";
        String username = "root";
        String password = "swap";
        ArrayList<String> list = new ArrayList();

        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
            list.add(resultSet.getString(1));
            System.out.println(resultSet.getString(1));
            }
            connection.close();
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
            }
            return list;
    }


         public String forgot_passwd(String email,String dob,String type) {
        String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "";
        if(type.equals("admin"))
        {
            query = "select * from team_leader_info where email="+"'"+email.trim()+"' and dob='"+dob.trim()+"'";
        }
        else if(type.equals("emp"))
        {
            query = "select * from db_register where email="+"'"+email.trim()+"' and dob='"+dob.trim()+"'";
        }
        String username = "root";
        String password = "swap";
        String status="";
        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
            if(resultSet.getString(3).equals(email) && resultSet.getString(4).equals(dob))
            {
                status = "sent";
                SendMail(email,"Your Password Is: "+resultSet.getString(2),"Password Sent By LiveMeetUp.");
                
            System.out.println(status);
            }
            else{
            status = "not";
            }

            }
            connection.close();
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
            }
            return status;
    }



      public String selectdata(String usrname,String passwd) {
        String dbUrl = "jdbc:mysql://localhost/livemeet";
        String dbClass = "com.mysql.jdbc.Driver";
        String query = "select * from db_register where usrname="+"'"+usrname+"' and passwd='"+passwd+"'";
        String username = "root";
        String password = "swap";
        String status="";
        try {
            Class.forName(dbClass);
            Connection connection = DriverManager.getConnection(dbUrl,username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            
            if (resultSet.next()) {
            if(resultSet.getString(1).equals(usrname) && resultSet.getString(2).equals(passwd))
            {
                String session = Random64();
                status = "exist:"+session;
             addsession(usrname,session);
            System.out.println(status);
            }
            else{
            status = "not";
            }

            }
            connection.close();
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
            }
            return status;
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
         PrintWriter out = response.getWriter();
        out.println("Wow, I'm embedded!");

    } 

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

       String name=request.getParameter("usrname");
        String passwd=request.getParameter("passwd");
         String email=request.getParameter("email");
          String dob=request.getParameter("dob");
          String teamleader=request.getParameter("teamleader");
          
          if(request.getParameter("flag").equals("login"))
          {
         PrintWriter out = response.getWriter();

         out.println(selectdata(name,passwd));
        }
        else if(request.getParameter("flag").equals("register")){
    PrintWriter out = response.getWriter();
         out.println(insertdata(name,passwd,email,dob,teamleader));
              
        }

          else if(request.getParameter("flag").equals("admin_register")){
    PrintWriter out = response.getWriter();
         out.println(insertadmindata(name,passwd,email,dob));

        }

          else if(request.getParameter("flag").equals("admin_login")){
         
    PrintWriter out = response.getWriter();
         out.println(selectadmindata(name,passwd));

        }

          else if(request.getParameter("flag").equals("get_list")){
    PrintWriter out = response.getWriter();
         out.println(selectlist());

        }

          else if(request.getParameter("flag").equals("session_test")){

    PrintWriter out = response.getWriter();
         out.println(session_test(request.getParameter("session_id")));

        }

          else if(request.getParameter("flag").equals("logout")){

    PrintWriter out = response.getWriter();
         out.println(logout(request.getParameter("session_id")));

        }
          else if(request.getParameter("flag").equals("fill_user")){

    PrintWriter out = response.getWriter();
         out.println(fill_user(request.getParameter("usrname")));

        }
          else if(request.getParameter("flag").equals("invite")){

    PrintWriter out = response.getWriter();
         out.println(SendMail(request.getParameter("mail_list").toString(),request.getParameter("msg"),"You are Invited for meeting!"));

        }
          else if(request.getParameter("flag").equals("get_user_list")){

    PrintWriter out = response.getWriter();
         out.println(get_user_list());

        }
           else if(request.getParameter("flag").equals("forgot_emp")){

    PrintWriter out = response.getWriter();
         out.println(forgot_passwd(request.getParameter("email"),request.getParameter("dob"),"emp"));

        }
           else if(request.getParameter("flag").equals("forgot_admin")){

    PrintWriter out = response.getWriter();
         out.println(forgot_passwd(request.getParameter("email"),request.getParameter("dob"),"admin"));

        }
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
